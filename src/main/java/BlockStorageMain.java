import pl.pateman.blockstorage.BlockStorage;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by pateman.
 */
public class BlockStorageMain {
    public static final int BLOCK_SIZE = 32;
    public static final int NUMBER_OF_BLOCKS = 1024;
    public static final String EXAMPLE_STRING = "EXAMPLE_STRING";
    public static final String EXAMPLE_STRING_2 = "EXAMPLE_STRING2";

    public static void main(String[] args) {
        testMultiThreadedWrite();
        testRemove();
        testMultiThreadedReadAndWrite();
        testMultiThreadedReadAndRemove();
        testOverwrite();
    }

    private static void testRemove() {
        try (final BlockStorage blockStorage = new BlockStorage("testRemove.bin", BLOCK_SIZE * NUMBER_OF_BLOCKS,
                BLOCK_SIZE)) {
            final int startingBlock = blockStorage.write(EXAMPLE_STRING);
            final int startingBlock2 = blockStorage.write(EXAMPLE_STRING_2);

            if (startingBlock != 0 && startingBlock2 != 1) {
                throw new IllegalStateException("Incorrect block assignment on empty file");
            }

            String read = blockStorage.read(startingBlock, String.class);
            if (!EXAMPLE_STRING.equals(read)) {
                throw new IllegalStateException("Read error");
            }

            blockStorage.remove(startingBlock);
            read = blockStorage.read(startingBlock, String.class);
            if (read != null) {
                throw new IllegalStateException("Remove error");
            }

            read = blockStorage.read(startingBlock2, String.class);
            if (!EXAMPLE_STRING_2.equals(read)) {
                throw new IllegalStateException("Read after remove error");
            }

            if (blockStorage.write(EXAMPLE_STRING) != 0) {
                throw new IllegalStateException("Invalid non-dirty block was picked");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        new File("testRemove.bin").delete();
    }

    private static void testMultiThreadedWrite() {
        try (final BlockStorage blockStorage = new BlockStorage("testMultiThreadedWrite.bin",
                BLOCK_SIZE * NUMBER_OF_BLOCKS, BLOCK_SIZE)) {

            class Block {
                Integer block;
                long threadId;

                public Block(Integer block, long threadId) {
                    this.block = block;
                    this.threadId = threadId;
                }
            }

            final List<Block> startingBlocks = new ArrayList<>();

            for (int i = 0; i < 10; i++) {
                final String s = "String" + i;
                final int firstBlock = blockStorage.write(s);
                startingBlocks.add(new Block(firstBlock, Thread.currentThread().getId()));
            }


            class WritingThread implements Callable<Void> {
                @Override
                public Void call() throws Exception {
                    for (int i = 10; i < 20; i++) {
                        startingBlocks.add(new Block(blockStorage.write("String" + i), Thread.currentThread().getId()));
                    }
                    return null;
                }
            }

            final ExecutorService executorService = Executors.newFixedThreadPool(4);
            final Set<Callable<Void>> callables = new HashSet<>();
            callables.add(new WritingThread());
            callables.add(new WritingThread());
            callables.add(new WritingThread());
            callables.add(new WritingThread());

            final List<Future<Void>> futures = executorService.invokeAll(callables);
            boolean allDone = false;
            while (!allDone) {
                int numDone = 0;
                for (Future<Void> future : futures) {
                    if (future.isDone()) {
                        ++numDone;
                    }
                }
                if (numDone == 4) {
                    allDone = true;
                }
            }
            executorService.shutdown();

            Collections.sort(startingBlocks, (o1, o2) -> o1.block - o2.block);
            for (Block block : startingBlocks) {
                final String readString = blockStorage.read(block.block, String.class);
                System.out.printf("Block: %d (%d) - %s\n", block.block, block.threadId, readString);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        new File("testMultiThreadedWrite.bin").delete();
    }

    private static void testMultiThreadedReadAndWrite() {
        try (final BlockStorage blockStorage = new BlockStorage("testMultiThreadedReadAndWrite.bin",
                BLOCK_SIZE * NUMBER_OF_BLOCKS, BLOCK_SIZE)) {

            final Map<Integer, Integer> toc = new ConcurrentHashMap<>();
            final AtomicInteger atomicInteger = new AtomicInteger();

            class WritingThread implements Callable<Void> {
                private final int from;
                private final int to;

                WritingThread(int startFrom, int runTo) {
                    this.from = startFrom;
                    this.to = runTo;
                }

                @Override
                public Void call() throws Exception {
                    for (int i = this.from; i < this.to; i++) {
                        toc.put(i, blockStorage.write("String" + i));
                    }
                    return null;
                }
            }

            class ReadingThread implements Runnable {
                private final int readIndex;

                ReadingThread(int readIndex) {
                    this.readIndex = readIndex;
                }

                @Override
                public void run() {
                    Integer firstBlock = null;
                    while (firstBlock == null) {
                        firstBlock = toc.get(this.readIndex);
                    }

                    try {
                        System.out.println(blockStorage.read(firstBlock, String.class));
                    } catch (IOException | InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        atomicInteger.incrementAndGet();
                    }
                }
            }

            new Thread(new FutureTask<>(new WritingThread(0, 50))).start();
            new Thread(new ReadingThread(2)).start();
            new Thread(new ReadingThread(8)).start();
            new Thread(new ReadingThread(35)).start();
            new Thread(new ReadingThread(0)).start();

            while (atomicInteger.get() != 4) {
                Thread.sleep(10);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        new File("testMultiThreadedReadAndWrite.bin").delete();
    }

    private static void testMultiThreadedReadAndRemove() {
        try (final BlockStorage blockStorage = new BlockStorage("testMultiThreadedReadAndRemove.bin",
                20 * NUMBER_OF_BLOCKS, 16)) {
            final Map<Integer, Integer> items = new HashMap<>(100);
            for (int i = 0; i < 100; i++) {
                items.put(i, blockStorage.write("Some longer string" + i));
            }
            System.out.printf("** BEFORE REMOVE**\nTotal space: %d, Free space: %d\n", blockStorage.getStorageSize(),
                    blockStorage.getFreeSpace());

            class ReadingThread implements Callable<String> {
                private final int idx;
                private final int sleep;

                ReadingThread(int idx, int sleep) {
                    this.idx = idx;
                    this.sleep = sleep;
                }

                @Override
                public String call() throws Exception {
                    try {
                        Thread.sleep(this.sleep * 10);
                        return blockStorage.read(this.idx, String.class);
                    } catch (Exception ex) {
                        System.out.println(ex.getMessage());
                        return null;
                    }
                }
            }

            class RemovingThread implements Callable<Void> {
                private final List<Integer> blocksToRemove;

                RemovingThread(List<Integer> blocksToRemove) {
                    this.blocksToRemove = blocksToRemove;
                }


                @Override
                public Void call() throws Exception {
                    try {
                        for (Integer blockToRemove : blocksToRemove) {
                            blockStorage.remove(blockToRemove);
                            Thread.sleep(1);
                        }
                    } catch (Exception ex) {
                        System.out.println(ex.getMessage());
                    }
                    return null;
                }
            }

            final ExecutorService executorService = Executors.newFixedThreadPool(4);
            final List<Future<String>> futures = new ArrayList<>(10);
            final Future<Void> future1 = executorService.submit(new RemovingThread(new ArrayList<>(items.values())));
            for (int i = 0; i < 10; i++) {
                final Future<String> future = executorService.submit(new ReadingThread(items.get(i * 10), i));
                futures.add(future);
            }

            while (true) {
                final List<String> results = new ArrayList<>(10);
                for (Future<String> future : futures) {
                    if (future.isDone()) {
                        results.add(future.get());
                    }
                }

                if (results.size() == 10 && future1.isDone()) {
                    System.out.println(results);
                    System.out.printf("Total space: %d, Free space: %d\n", blockStorage.getStorageSize(),
                            blockStorage.getFreeSpace());
                    break;
                }
            }

            executorService.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }

        new File("testMultiThreadedReadAndRemove.bin").delete();
    }

    private static void testOverwrite() {
        try (final BlockStorage blockStorage = new BlockStorage("testOverwrite.bin", 20 * NUMBER_OF_BLOCKS, 16)) {
            class NonTrivialObject {
                private final int integer;
                private final float floatingPoint;

                public NonTrivialObject() {
                    this.integer = 0;
                    this.floatingPoint = 0.0f;
                }

                NonTrivialObject(int integer, float floatingPoint) {
                    this.integer = integer;
                    this.floatingPoint = floatingPoint;
                }

                public int getInteger() {
                    return integer;
                }

                public float getFloatingPoint() {
                    return floatingPoint;
                }

                @Override
                public String toString() {
                    return "NonTrivialObject{" +
                            "integer=" + integer +
                            ", floatingPoint=" + floatingPoint +
                            '}';
                }
            }

            final NonTrivialObject nonTrivialObject1 = new NonTrivialObject(1992, 3.1415f);
            final NonTrivialObject nonTrivialObject2 = new NonTrivialObject(1994, 6.283f);

            final int block = blockStorage.write(nonTrivialObject1);
            NonTrivialObject read = blockStorage.read(block, NonTrivialObject.class);
            System.out.println("Before overwrite: " + read);

            blockStorage.overwriteBlock(block, nonTrivialObject2);
            read = blockStorage.read(block, NonTrivialObject.class);
            System.out.println("After overwrite: " + read);
        } catch (Exception e) {
            e.printStackTrace();
        }

        new File("testOverwrite.bin").delete();
    }
}
