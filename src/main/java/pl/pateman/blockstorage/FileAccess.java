package pl.pateman.blockstorage;

import java.io.*;

/**
 * Created by pateman.
 */
class FileAccess implements AutoCloseable {
    private final RandomAccessFile randomAccessFile;

    public FileAccess(final String file) throws FileNotFoundException {
        this.randomAccessFile = new RandomAccessFile(file, "rw");
    }

    public void flush() throws IOException {
        this.randomAccessFile.getFD().sync();
    }

    public void setPosition(long newPosition) throws IOException {
        this.randomAccessFile.seek(newPosition);
    }

    public InputStream getInputStream() {
        return new BufferedInputStream(new FileAccessInputStream());
    }

    public long getPosition() throws IOException {
        return this.randomAccessFile.getFilePointer();
    }

    public boolean isEmpty() throws IOException {
        return this.randomAccessFile.length() == 0;
    }

    public void write(byte b[], int off, int len) throws IOException {
        this.randomAccessFile.write(b, off, len);
    }

    public int read(byte b[], int off, int len) throws IOException {
        return this.randomAccessFile.read(b, off, len);
    }

    public void writeInt(int integer) throws IOException {
        this.randomAccessFile.writeInt(integer);
    }

    @Override
    public void close() throws Exception {
        this.randomAccessFile.getFD().sync();
        this.randomAccessFile.close();
    }

    private final class FileAccessInputStream extends InputStream {
        @Override
        public int read() throws IOException {
            return FileAccess.this.randomAccessFile.read();
        }

        @Override
        public int available() throws IOException {
            return Math.toIntExact(FileAccess.this.randomAccessFile.length());
        }
    }
}
