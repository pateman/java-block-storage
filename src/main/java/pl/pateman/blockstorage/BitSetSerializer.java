package pl.pateman.blockstorage;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import java.util.BitSet;

/**
 * Created by pateman.
 */
class BitSetSerializer extends Serializer<BitSet> {

    @Override
    public void write(Kryo kryo, Output output, BitSet object) {
        final byte[] byteArray = object.toByteArray();

        output.writeInt(byteArray.length);
        output.write(byteArray);
    }

    @Override
    public BitSet read(Kryo kryo, Input input, Class<BitSet> type) {
        final int length = input.readInt();
        final byte[] bytes = input.readBytes(length);
        return BitSet.valueOf(bytes);
    }
}
