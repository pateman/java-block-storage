package pl.pateman.blockstorage;

/**
 * Created by pateman.
 */
class TrivialLock {
    private boolean isLocked = false;
    private final int lockTimeout;

    public TrivialLock() {
        this(0);
    }

    public TrivialLock(int timeout) {
        this.lockTimeout = timeout;
    }

    public synchronized void lock() throws InterruptedException {
        while (this.isLocked) {
            this.wait(this.lockTimeout);
        }
        this.isLocked = true;
    }

    public synchronized void unlock() {
        this.isLocked = false;
        this.notify();
    }
}
