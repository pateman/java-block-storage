package pl.pateman.blockstorage;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoException;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.BitSet;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Thread-safe block storage implementation. Uses the Kryo library under the hood to serialize/deserialize data.
 *
 * Layout of the data:
 *  ---------------------------------------------------
 * | BLOCKS BITSET | BLOCK 0 | BLOCK 1 | ... | BLOCK N |
 *  ---------------------------------------------------
 *
 * The bitset is basically an array of flags that denotes which blocks are either free or dirty. It is used to determine
 * where new data should be placed in the file. When inserting an object, it is serialized to a byte array, split into
 * chunks of {@code blockSize} bytes and persisted.
 *
 * Each block consists of actual object's data and metadata at the end of the block:
 *
 * ----------------------------------------------------------------
 * | BYTES OF DATA | DATA LENGTH (4 BYTES) | NEXT BLOCK (4 BYTES) |
 * ----------------------------------------------------------------
 *
 * Data length determines how many bytes the actual object's data occupies in the block. Next block is an integer that
 * determines which block the reader should go to next in order to get another piece of data. If the next block is set
 * to {@code INVALID_BLOCK}, it means that there's no more blocks to go to and we've reached the end of the data.
 *
 * Created by pateman.
 */
public final class BlockStorage implements AutoCloseable {
    private static final int LOCK_TIMEOUT = 1000;
    private static final int INT_SIZE = 4;
    private static final boolean FREE_BLOCK = true;
    private static final boolean DIRTY_BLOCK = false;
    private static final byte BLOCK_METADATA_SIZE = 8;
    private static final byte MIN_BLOCK_SIZE = 16;
    private static final int INVALID_BLOCK = -1;

    private final FileAccess fileAccess;
    private final TrivialLock readLock = new TrivialLock(LOCK_TIMEOUT);
    private final TrivialLock writeLock = new TrivialLock(LOCK_TIMEOUT);
    private final Kryo kryo;
    private final int storageSize;
    private final int blockSize;
    private final int numberOfBlocks;
    private final int dataPerBlock;
    private final AtomicInteger freeBlocks;
    private int headerLength;
    private BitSet blockBits;

    public BlockStorage(final String fileName, int storageSize, int blockSize) throws Exception {
        this.storageSize = storageSize;
        this.blockSize = blockSize;
        this.numberOfBlocks = Math.floorDiv(this.storageSize, this.blockSize);
        if (this.blockSize < MIN_BLOCK_SIZE) {
            throw new IllegalArgumentException("Block size needs to be at least " + MIN_BLOCK_SIZE + " bytes");
        }
        this.dataPerBlock = this.blockSize - BLOCK_METADATA_SIZE;

        this.kryo = new Kryo();
        this.kryo.addDefaultSerializer(BitSet.class, new BitSetSerializer());
        this.fileAccess = new FileAccess(fileName);

        //  Initialize the block's bitset and calculate the number of free blocks.
        this.initializeBlockBits();

        int freeBlocks = 0;
        for (int i = 0; i < this.blockBits.length(); i++) {
            if (this.blockBits.get(i) == FREE_BLOCK) {
                freeBlocks++;
            }
        }
        this.freeBlocks = new AtomicInteger(freeBlocks);
    }

    /**
     * Moves the file pointer to the given block. If an invalid block number is given, an
     * {@code IllegalArgumentException} is thrown.
     *
     * @param blockNumber Block index to move to.
     * @throws IOException
     */
    private void moveToBlock(int blockNumber) throws IOException {
        if (blockNumber < 0 || blockNumber > this.numberOfBlocks) {
            throw new IllegalArgumentException("Incorrect block number");
        }

        this.fileAccess.setPosition(this.headerLength + (blockNumber * this.blockSize));
    }

    /**
     * Flags the given block.
     *
     * @param block Block number.
     * @param flag Block flag.
     */
    private void markBlock(int block, boolean flag) {
        this.blockBits.set(block, flag);
        if (flag) {
            this.freeBlocks.incrementAndGet();
        } else {
            this.freeBlocks.decrementAndGet();
        }
    }

    /**
     * Returns an array of blocks that match the given flag. This method performs in O(n).
     *
     * @param numberOfBlocks Number of blocks to look for.
     * @param flag Block flag.
     * @return Array of blocks that match the given flag.
     */
    private int[] getBlocks(int numberOfBlocks, boolean flag) {
        final List<Integer> blocksList = new LinkedList<>();

        for (int i = 0; i < this.blockBits.length(); i++) {
            if (numberOfBlocks <= 0) {
                break;
            }

            if (this.blockBits.get(i) == flag) {
                blocksList.add(i);
                numberOfBlocks--;
            }
        }

        return blocksList.
                stream().
                mapToInt(Integer::intValue).
                toArray();
    }

    /**
     * Accesses the given block and reads its data to a byte array.
     *
     * @param data Byte array which holds the block's data.
     * @param blockIndex Block index.
     * @return Number of bytes which were actually read from the file.
     * @throws IOException
     */
    private int readBlock(final byte[] data, int blockIndex) throws IOException {
        this.moveToBlock(blockIndex);
        return this.fileAccess.read(data, 0, this.blockSize);
    }

    /**
     * Writes the blocks' bitset to the disk.
     *
     * @return The total length of the blocks' bitset.
     * @throws IOException
     * @throws InterruptedException
     */
    private int persistBlockBits() throws IOException, InterruptedException {
        return this.persistBlockBits(true);
    }

    /**
     * Writes the blocks' bitset to the disk.
     *
     * @param lock Determines whether the method should apply a write lock.
     * @return The total length of the blocks' bitset.
     * @throws IOException
     * @throws InterruptedException
     */
    private int persistBlockBits(final boolean lock) throws IOException, InterruptedException {
        if (lock) {
            writeLock.lock();
        }
        try {
            try (final Output output = new Output(new ByteArrayOutputStream())) {
                this.serialize(output, this.blockBits);

                final byte[] outputBuffer = output.getBuffer();

                this.fileAccess.setPosition(0L);
                this.writeToFile(outputBuffer, 0, outputBuffer.length);

                return outputBuffer.length;
            }
        } finally {
            if (lock) {
                writeLock.unlock();
            }
        }
    }

    /**
     * Initializes the blocks' bitset.
     *
     * @throws IOException
     * @throws InterruptedException
     */
    private void initializeBlockBits() throws IOException, InterruptedException {
        if (this.fileAccess.isEmpty()) {
            this.blockBits = new BitSet(this.numberOfBlocks);
            this.blockBits.set(0, this.numberOfBlocks, true);

            this.headerLength = this.persistBlockBits();
        } else {
            try (final Input input = new Input(this.fileAccess.getInputStream())) {
                this.blockBits = this.deserialize(input, BitSet.class);
            }
            this.headerLength = Math.toIntExact(this.fileAccess.getPosition());
        }
    }

    /**
     * Launches Kryo to serialize the given object to output.
     *
     * @param output Output stream.
     * @param o Object to serialize.
     * @throws IOException
     */
    private void serialize(final Output output, final Object o) throws IOException {
        try {
            this.kryo.writeObject(output, o);
        } catch (Exception ex) {
            throw new IOException(ex);
        }
    }

    /**
     * Writes data to file.
     *
     * @param b Array of bytes.
     * @param off Offset from which the data should be written.
     * @param len Number of bytes to write.
     * @throws IOException
     */
    private void writeToFile(byte b[], int off, int len) throws IOException {
        this.fileAccess.write(b, off, len);
    }

    /**
     * Launches Kryo to deserialize an object from the given input stream.
     *
     * @param input Input stream.
     * @param type Deserialized object's class.
     * @param <T>
     * @return Deserialized object.
     * @throws IOException
     */
    private <T> T deserialize(Input input, Class<T> type) throws IOException {
        try {
            return this.kryo.readObject(input, type);
        } catch (KryoException ex) {
            return null;
        } catch (Exception ex) {
            throw new IOException(ex);
        }
    }

    /**
     * Writes metadata to the given block data array.
     *
     * @param blockData Block data.
     * @param len Actual data length.
     * @param nextBlock Pointer to the next block.
     */
    private void writeMetaData(final byte[] blockData, final int len, final int nextBlock) {
        blockData[this.blockSize - 8] = (byte) ((len >>> 24) & 0xFF);
        blockData[this.blockSize - 7] = (byte) ((len >>> 16) & 0xFF);
        blockData[this.blockSize - 6] = (byte) ((len >>> 8) & 0xFF);
        blockData[this.blockSize - 5] = (byte) ((len >>> 0) & 0xFF);

        blockData[this.blockSize - 4] = (byte) ((nextBlock >>> 24) & 0xFF);
        blockData[this.blockSize - 3] = (byte) ((nextBlock >>> 16) & 0xFF);
        blockData[this.blockSize - 2] = (byte) ((nextBlock >>> 8) & 0xFF);
        blockData[this.blockSize - 1] = (byte) ((nextBlock >>> 0) & 0xFF);
    }

    /**
     * Reads the next block pointer from the given block.
     *
     * @param blockNumber Block to read the pointer from.
     * @return Next block pointer.
     * @throws IOException
     */
    private int getNextBlockPointerFromBlock(final int blockNumber) throws IOException {
        final byte[] data = new byte[INT_SIZE];

        //  Move to the current's block metadata where the next block is stored.
        this.fileAccess.setPosition(this.headerLength + (blockNumber * this.blockSize) + this.dataPerBlock +
                INT_SIZE);

        //  Read the next block.
        final int readBytes = this.fileAccess.read(data, 0, INT_SIZE);
        if (readBytes < INT_SIZE) {
            throw new IOException("Insufficient data read. Unable to remove");
        }

        return ByteBuffer.wrap(data).getInt();
    }

    /**
     * Persists the given object to the storage.
     *
     * @param object Object to persist.
     * @return Index of the first block where the object was placed.
     * @throws IOException
     * @throws InterruptedException
     */
    public int write(final Object object) throws IOException, InterruptedException {
        if (object == null) {
            throw new IllegalArgumentException("Unable to write a null value");
        }

        int result = INVALID_BLOCK;
        try (final Output output = new Output(new ByteArrayOutputStream())) {
            //  Serialize the object to a byte array first.
            this.serialize(output, object);
            final byte[] outputBuffer = output.getBuffer();

            //  Calculate the number of blocks needed to store the object in the storage.
            int numberOfBlocksNeeded = (int) Math.ceil((double) output.total() / this.dataPerBlock);
            final int[] blocks;

            //  Query the blocks' bitset to find which blocks are available for storing the object.
            readLock.lock();
            try {
                blocks = this.getBlocks(numberOfBlocksNeeded, FREE_BLOCK);

                //  We couldn't get the desired amount of blocks. Bail out.
                if (blocks.length < numberOfBlocksNeeded) {
                    throw new IllegalStateException("Not enough blocks in the storage");
                }

                //  While still in the read lock, mark the blocks as dirty.
                for (int blockNumber : blocks) {
                    this.markBlock(blockNumber, DIRTY_BLOCK);
                }
            } finally {
                readLock.unlock();
            }

            long remaining = output.total();
            int lastOffset = 0;
            final byte[] blockData = new byte[this.blockSize];
            for (int block = 0; block < numberOfBlocksNeeded; block++) {
                //  Determine the next block's index.
                final int blockNumber = blocks[block];
                final int len = Math.toIntExact(Math.min(this.dataPerBlock, remaining));
                final int nextBlock = block == numberOfBlocksNeeded - 1 ? INVALID_BLOCK : blocks[block + 1];

                writeLock.lock();
                try {
                    //  Write data that fits.
                    System.arraycopy(outputBuffer, lastOffset, blockData, 0, len);
                    lastOffset += len;

                    //  Persist block's metadata as well.
                    this.writeMetaData(blockData, len, nextBlock);

                    //  Write to file.
                    this.moveToBlock(blockNumber);
                    this.writeToFile(blockData, 0, this.blockSize);

                    remaining -= len;
                } catch (Exception ex) {
                    //  Revert the blocks and re-throw the exception.
                    readLock.lock();
                    try {
                        for (int b : blocks) {
                            this.markBlock(b, FREE_BLOCK);
                        }
                    } finally {
                        readLock.unlock();
                    }

                    throw new IOException(ex);
                } finally {
                    writeLock.unlock();
                }
            }

            //  Persist the blocks' bitset.
            result = blocks[0];
            this.headerLength = this.persistBlockBits();
        }

        return result;
    }

    /**
     * Overwrites the given block with new data.
     *
     * The block that you want to overwrite must be a dirty one, plus the data that you're passing needs to fit in
     * the block, otherwise an exception will be thrown.
     *
     * This method does not change the next block pointer, which may lead to data corruption. Use with care :)
     *
     * @param block Block to overwrite.
     * @param object New data to write to the block.
     * @throws InterruptedException
     * @throws IOException
     */
    public void overwriteBlock(int block, final Object object) throws InterruptedException, IOException {
        if (object == null) {
            throw new IllegalArgumentException("Unable to write a null value");
        }

        if (block < 0 || block > this.numberOfBlocks) {
            throw new IllegalArgumentException("Incorrect block");
        }

        //  Check if the block that we want to override is marked as dirty. If it's not, bail out.
        readLock.lock();
        try {
            if (this.blockBits.get(block) != DIRTY_BLOCK) {
                throw new IllegalArgumentException("The given block " + block + " is not dirty. Cannot overwrite");
            }
        } finally {
            readLock.unlock();
        }

        try (final Output output = new Output(new ByteArrayOutputStream())) {
            this.serialize(output, object);

            //  Check if the data fits into the block. If not, throw an exception.
            final int blocksNeeded = (int) Math.ceil((double) output.total() / this.dataPerBlock);
            if (blocksNeeded > 1) {
                throw new IOException("Data will not fit into the block");
            }

            final byte[] outputBuffer = output.getBuffer();
            final byte[] blockData = new byte[this.blockSize];
            final int len = Math.toIntExact(Math.min(this.dataPerBlock, output.total()));

            readLock.lock();
            writeLock.lock();
            try {
                //  Read the current's block next block. We're going to overwrite only the data length, but we need to
                //  keep the next block pointer.
                final int nextBlockPointerFromBlock = this.getNextBlockPointerFromBlock(block);

                //  Prepare data.
                System.arraycopy(outputBuffer, 0, blockData, 0, len);
                this.writeMetaData(blockData, len, nextBlockPointerFromBlock);

                //  Write to file.
                this.moveToBlock(block);
                this.writeToFile(blockData, 0, this.blockSize);
            } finally {
                readLock.unlock();
                writeLock.unlock();
            }
        }
    }

    /**
     * Reads data of the given type from the starting block.
     *
     * @param startingBlock Block to start reading from.
     * @param clazz Object type.
     * @param <T>
     * @return Deserialized object.
     * @throws IOException
     * @throws InterruptedException
     */
    public <T> T read(final int startingBlock, final Class<T> clazz) throws IOException, InterruptedException {
        if (startingBlock < 0 || startingBlock > this.numberOfBlocks) {
            throw new IllegalArgumentException("Incorrect starting block");
        }

        int nextBlock = startingBlock;
        final byte[] block = new byte[this.blockSize];

        byte[] result = new byte[0];
        int totalSize = 0;

        //  Keep going from the starting block until we hit a block which is invalid.
        while (nextBlock != INVALID_BLOCK) {
            //  Check if the block is occupied. If not, break the loop.
            readLock.lock();
            writeLock.lock();
            try {
                if (this.blockBits.get(nextBlock) == FREE_BLOCK) {
                    break;
                }
            } finally {
                readLock.unlock();
                writeLock.unlock();
            }
            final int read;

            //  Read the block.
            readLock.lock();
            try {
                read = this.readBlock(block, nextBlock);
            } finally {
                readLock.unlock();
            }

            //  Have a look at the block's metadata, read the actual data amount and read the data.
            final int dataLength = ByteBuffer.wrap(new byte[]{
                    block[read - 8], block[read - 7], block[read - 6], block[read - 5]
            }).getInt();
            if (dataLength > 0) {
                result = Arrays.copyOf(result, result.length + dataLength);
                System.arraycopy(block, 0, result, totalSize, dataLength);
                totalSize += dataLength;
            }

            //  Look at the metadata once again and get the next block.
            nextBlock = ByteBuffer.wrap(new byte[]{
                    block[read - 4], block[read - 3], block[read - 2], block[read - 1]
            }).getInt();
        }

        //  We've gathered the object's byte array. Deserialize it.
        try (final Input input = new Input(new ByteArrayInputStream(result))) {
            return this.deserialize(input, clazz);
        }
    }

    /**
     * Removes data starting from the given block.
     *
     * Data is not actually removed - all blocks which are occupied in the chain are marked as free again. This method
     * is costly as it locks the storage for both reading and writing until removal completes.
     *
     * @param startingBlock Block to start from.
     * @throws IOException
     */
    public void remove(final int startingBlock) throws IOException {
        try {
            readLock.lock();
            writeLock.lock();

            final List<Integer> blocksToRemove = new LinkedList<>();
            int nextBlock = startingBlock;
            while (nextBlock != INVALID_BLOCK) {
                //  Check if the block is dirty. If it's not, throw an exception.
                if (this.blockBits.get(nextBlock) != DIRTY_BLOCK) {
                    throw new IOException("The block " + nextBlock + " is free");
                }

                //  Add the block to the list of blocks which should be removed.
                blocksToRemove.add(nextBlock);

                //  Read the next block pointer from the block.
                nextBlock = this.getNextBlockPointerFromBlock(nextBlock);
            }

            //  Mark the blocks as free and persist.
            for (Integer block : blocksToRemove) {
                this.markBlock(block, FREE_BLOCK);
            }
            this.headerLength = this.persistBlockBits(false);
        } catch (InterruptedException ex) {
            throw new IOException(ex);
        } finally {
            readLock.unlock();
            writeLock.unlock();
        }
    }

    /**
     * Registers a Kryo serializer for the given class.
     *
     * Kryo handles most of the classes really well, but just in case there are problems, you can write your own
     * serializers and deserializers. In order for Kryo to pick up your serializer, it needs to be registered with it.
     *
     * @param type Class.
     * @param serializer Serializer instance.
     * @param <T>
     */
    public <T> void registerSerializerForType(final Class<T> type, final Serializer<T> serializer) {
        if (type == null || serializer == null) {
            throw new IllegalArgumentException("Valid type and serializer need to be provided");
        }

        this.kryo.addDefaultSerializer(type, serializer);
    }

    public int getStorageSize() {
        return storageSize;
    }

    public int getBlockSize() {
        return blockSize;
    }

    public int getNumberOfBlocks() {
        return numberOfBlocks;
    }

    public long getFreeSpace() {
        return this.freeBlocks.get() * this.blockSize;
    }

    public void flush() throws IOException {
        this.fileAccess.flush();
    }

    @Override
    public void close() throws Exception {
        this.fileAccess.close();
    }
}
